# Proyecto

Este proyecto se generó con [Angular CLI](https://github.com/angular/angular-cli) version 7.2.0

#Como iniciar el proyecto

**1.Clonar este repositorio en Local:**

git clone git@bitbucket.org:duidac/marvel_app.git

**2.Entrar en el directorio del proyecto**

cd marvel_app\project

**3.Instalar dependencias**

npm install

**Servido de Desarrollo**
Ejecute `ng serve` para un servidor de desarrollo. Navegue a `http: // localhost: 4200 /`.
La aplicación se volverá a cargar automáticamente si cambia alguno de los archivos de origen.

## Build

Ejecute `ng build` para compilar el proyecto. 

#Documentación del Proyecto

La documentación del proyecto fue generada con la Herramienta Compodoc.

Para ver la documentación ejecute los siguientes pasos:

**Comando de instalación Compodoc**

Instalación Local:
npm install --save-dev @compodoc/compodoc

**Definir una tarea de script pen el package.json (con npm 6.x):**

"scripts": {
  "compodoc": "npx compodoc -p src/tsconfig.app.json"
}

**Ejecutar**

npm run compodoc


**Visualizar la documentación**

La herramienta genera dentro del proyecto  una carpeta  con el nombre :**documentation**
