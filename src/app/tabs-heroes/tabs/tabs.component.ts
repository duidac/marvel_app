import { Component, OnInit, Input } from '@angular/core';
import { MarvelService } from '../../_servicio/marvel.service';



@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit {
  @Input() list1: any;
    constructor(private MarvelSvc: MarvelService) { }
  public info;

  ngOnInit() {

        }

  /**Tabla de Busquedas
   * 1. Con el evento buscar enviar el nombre del super heroe
     */
      /** Busqueda de superheroe  :
     * @param name nombre del superheroe
     */
  public buscar(name: any): void {
    name = document.getElementById(" name ");
    this.MarvelSvc.getNameCharacters(name).subscribe((data: any) => {
      if (data.data.results === '' || data.data.results === undefined) {
        /*mensaje de error, no existe*/
      } else {
        this.info = data.data.results;
        /*Pasar la data a la tabla*/
      }
    });
  }
  /**Tabla completa
   *
   1.Recibe el listado de toda la data
   */

  public getcharacters(): void {
    /*Pintar tabla */

  }

}

