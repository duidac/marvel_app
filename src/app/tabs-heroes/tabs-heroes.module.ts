import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TableModule} from 'primeng/table';
import {TabViewModule} from 'primeng/tabview';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TableModule,
    TabViewModule
  ]
})
export class TabsHeroesModule { }
