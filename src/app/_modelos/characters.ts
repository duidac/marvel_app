export class Heroe {
  id: number;
  nombre: string;
  descripcion: string;
  token: string;
  path: string;
  extension: string;

}
