import { Component, OnInit } from '@angular/core';
import {MarvelService} from '../_servicio/marvel.service';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(private MarvelSvc: MarvelService) { }
    public characters;

  ngOnInit() {
  this._getCharacters();
  }

  /**
   * Consulta Listado de SuperHeroes
   */
    public _getCharacters(){
     this.MarvelSvc.getAllCharacters().subscribe( (data: any) => {
        this.characters = data.data.results;
           },
     (error) => {

      console.error(error);
     }
      );

      }
}
