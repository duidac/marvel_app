import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WelcomeComponent } from './welcome.component';
import { NavComponent} from '../navbar/nav/nav.component';
import { CardComponent} from '../card-heroes/card/card.component';
import {TabsComponent} from '../tabs-heroes/tabs/tabs.component';
import {TableModule} from 'primeng/table';
import {TabViewModule} from 'primeng/tabview';



@NgModule({
  declarations: [WelcomeComponent,NavComponent,CardComponent,TabsComponent],
  imports: [
    CommonModule,
    TableModule,
    TabViewModule

  ]
})
export class WelcomeModule { }
