import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment} from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})

export class MarvelService {

  private  baseurl = environment.marvel;

  PUBLIC_KEY = this.baseurl.public_key;
  HASH = this.baseurl.HASH;
  name= '';
  private URL_API_GET = '';

   URL_API = `https:gateway.marvel.com/v1/public/characters?ts=2&apikey=${this.PUBLIC_KEY}&hash=${this.HASH}`;

  constructor(private http: HttpClient) { }

  getAllCharacters(){
  return  this.http.get(this.URL_API);

  }

 /**
   * @param name nombre del superheroe
   */

  getNameCharacters (name) {
  this.URL_API_GET = `https:gateway.marvel.com/v1/public/characters?${this.name}&ts=2&apikey=${this.PUBLIC_KEY}&hash=${this.HASH}`;
  return  this.http.get(this.URL_API_GET);
}


}

