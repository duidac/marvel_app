import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import { WelcomeComponent} from './welcome/welcome.component';

const routes: Routes = [

  { path: '', component: WelcomeComponent },
  { path: 'login', component: LoginComponent },

  { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(routes);
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
