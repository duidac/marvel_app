import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {AutenticacionService} from './_servicio/autenticacion.service';
import { User} from './_modelos';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Marvel-Heroes';
  currentUser : User;

  constructor(
    private router: Router,
    private autenticationService: AutenticacionService
) {
    this.autenticationService.currentUser.subscribe(x => this.currentUser = x);
}

logout() {
    this.autenticationService.logout();
    this.router.navigate(['/login']);
}
}
